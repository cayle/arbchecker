const gtt = require('gdax-trading-toolkit');
const CCXTExchangeWrapper = require('gdax-trading-toolkit/build/src/exchanges/ccxt').default;
const events = require('events');

const FX_REFRESH_RATE = process.env.FX_REFRESH_RATE || 30 * 60 * 1000;
const CRYPTO_REFRESH_RATE = process.env.CRYPTO_REFRESH_RATE || 60 * 1000;

const logger = gtt.utils.ConsoleLoggerFactory();
const ee = new events.EventEmitter();

function startFX() {
    
    const fxService = gtt.Factories.SimpleFXServiceFactory('openexchangerates', logger, FX_REFRESH_RATE);
    fxService.addCurrencyPair({from: 'USD', to: 'ZAR'});
    fxService.on('FXRateUpdate', rates => {
        const rate = rates['USD-ZAR'].rate.toNumber();
        ee.emit('fx_update', rate);
    });
}

function startCrypto() {
    const auth = { key: null, passphrase: null, secret: null };
    const bitstamp = {
        id: 'bitstamp',
        api: CCXTExchangeWrapper.createExchange('bitstamp', auth, logger, auth),
        product: 'BTC-USD'
    };
    const luno = {
        id: 'luno',
        api: CCXTExchangeWrapper.createExchange('luno', auth, logger, auth),
        product: 'BTC-ZAR'
    };
    
    async function updateRates() {
        try {
            const rates = await fetchRates([bitstamp, luno]);
            ee.emit('crypto_update', rates);
        } catch (err) {
            logger.log('error', 'Error fetching crypto rates', err.message);
        }
    }
    
    updateRates();
    setInterval(updateRates, CRYPTO_REFRESH_RATE);
}

async function fetchRates(exchanges) {
    const promises = [];
    exchanges.forEach(exchange => {
        promises.push(exchange.api.loadMidMarketPrice(exchange.product));
    });
    return Promise.all(promises).then(results => {
        const prices = {};
        for (let i = 0; i < results.length; i++) {
            const ex = exchanges[i];
            prices[ex.id] = {price: results[i].toNumber()};
        }
        return prices;
    }).catch(err => {
        return {error: err.message};
    });
}

function startArbChecker() {
    startCrypto();
    startFX();
    const arb = {
        exchangeRate: null,
        lunoZAR: null,
        lunoUSD: null,
        bitstampUSD: null,
        ratio: null,
        diff: null
    };
    
    function recalc() {
        let newPrice;
        try {
            newPrice = arb.lunoZAR / arb.exchangeRate;
        } catch (err) {
            logger.log('error', 'Could not calculate new lunoUSD price', arb);
            return;
        }
        arb.lunoUSD = newPrice;
        arb.ratio = arb.lunoUSD / arb.bitstampUSD;
        arb.diff = arb.lunoUSD - arb.bitstampUSD;
        let isValid;
        try {
            isValid = isFinite(arb.exchangeRate) && isFinite(arb.lunoZAR) &&
                isFinite(arb.lunoUSD) && isFinite(arb.bitstampUSD) &&
                isFinite(arb.ratio) && isFinite(arb.diff);
        } catch (err) {
            isValid = false;
        }
        if (isValid) {
            ee.emit('new_price', arb);
            logger.log('info', 'New price information', arb);
        } else {
            logger.log('warn', 'Some prices are invalid', arb);
        }
    }
    
    ee.on('crypto_update', msg => {
        logger.log('info', 'Crypto update received');
        arb.lunoZAR = msg.luno.price;
        arb.bitstampUSD = msg.bitstamp.price;
        recalc();
    });
    
    ee.on('fx_update', rate => {
        logger.log('info', `New FX rate received: ${rate}`);
        if (isFinite(rate)) {
            arb.exchangeRate = rate;
            recalc();
        } else {
            logger.log('error', 'Invalid USD-ZAR exchange rate received', rate);
        }
    });
}

module.exports = {startArbChecker, priceEvents: ee};
