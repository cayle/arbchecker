const GTT = require('gdax-trading-toolkit');
const logger = GTT.utils.ConsoleLoggerFactory();
const pusher = new GTT.utils.PushBullet(process.env.PUSHBULLET_KEY);
const deviceID = process.env.PUSHBULLET_DEVICE_ID;

function pushMessage(title, msg) {
    pusher.note(deviceID, title, msg, (err, res) => {
        if (err) {
            logger.log('error', 'Push message failed', err);
            return;
        }
        logger.log('info', 'Push message result', res);
    });
}

module.exports = { pushMessage };
