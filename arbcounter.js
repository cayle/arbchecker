'use strict';

const app = require('express')();
const checker = require('./lib/priceChecker');
const alerts = require('./lib/alerts');

checker.startArbChecker();

const prices = {};
const highAlert = process.env.HIGH_ALERT || 1.10;
const lowAlert = process.env.LOW_ALERT || 0.95;
let lastAlert = 0;
const TEN_MINUTES = 10 * 60 * 1000;
let priceTable = '';

function sendAlerts(prices) {
    if (Date.now() - lastAlert < TEN_MINUTES) {
        return;
    }
    lastAlert = Date.now();
    const ratio = prices.ratio;
    const msg = `Luno     USD: \$${prices.lunoUSD.toFixed(2)}\nBitstamp USD: \$${prices.bitstampUSD.toFixed(2)}\nRatio: ${prices.ratio.toFixed(2)}`;
    if (ratio >= highAlert) {
        alerts.pushMessage('Relative BTC price is HIGH', msg);
    }
    if (ratio <= lowAlert) {
        alerts.pushMessage('Relative BTC price is LOW', msg);
    }
}

function updatePriceTable(prices) {
    priceTable = `<html>
<body>
    <table style="border-style: solid">
        <tr style="background-color: cadetblue"><th>Bitstamp</th><td>\$</td><td>${prices.bitstampUSD.toFixed(2)}</td></tr>
        <tr style="background-color: azure"><th>Luno</th><td>R</td><td>${prices.lunoZAR.toFixed(2)}</td></tr>
        <tr style="background-color: cadetblue"><th>Luno</th><td>$</td><td>${prices.lunoUSD.toFixed(2)}</td></tr>
        <tr style="background-color: azure"><th>Ratio</th><td></td><td>${prices.ratio.toFixed(3)}</td></tr>
        <tr style="background-color: cadetblue"><th>Exchange rate</th><td>$</td><td>${prices.exchangeRate.toFixed(2)}</td></tr>
        <tr style="background-color: azure"><th>Difference</th><td>$</td><td>${prices.diff.toFixed(2)}</td></tr>
    </table>
</body>
</html>`;
}

checker.priceEvents.on('new_price', msg => {
    Object.assign(prices, msg);
    sendAlerts(prices);
    updatePriceTable(prices);
});

app.get('/health', (req, res) => {
    res.json({status: true});
});

app.get('/prices', (req, res) => {
    res.json(prices);
});

app.get('/', (req, res) => {
    res.status(200).send(priceTable);
});

const port = process.env.HTTP_PORT || 8005;
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

process.on('SIGTERM', () => {
    console.log('bye');
    process.exit(0);
});
