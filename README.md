# Run
`node arbchecker`

# ENVARS
* OPENEXCHANGE_API_KEY for exchange rates
* PUSHBULLET_KEY for alerts
* PUSHBULLET_DEVICE_ID for alerts
* HTTP_PORT (optional) defaults to 8005
* CRYPTO_REFRESH_RATE interval for refetching crypto prices (1min)
* FX_REFRESH_RATE interval for refetching fiat prices (20min)
* HIGH_ALERT ratio to alert at (1.10)
* LOW_ALERT ratio to alert at (0.95)

# Endpoints

* `/health` returns `{status: true}`
* `prices` returns latest prices
