FROM node:9.6

RUN mkdir -p /app/lib
WORKDIR /app
ADD package.json /app/
RUN npm install
ADD lib /app/lib
ADD arbcounter.js README.md /app/

EXPOSE 8005
CMD ["node", "arbcounter"]

