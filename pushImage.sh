#!/usr/bin/env bash
VER=1.0
docker build -t nimbustech/arbchecker:$VER .
docker push nimbustech/arbchecker:$VER
